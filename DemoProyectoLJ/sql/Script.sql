/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  allanmual
 * Created: 25-mar-2019
 */

CREATE DATABASE taller_mecanico_lj
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE taller_mecanico_lj
    IS 'Base de Datos para el Demo del Proyecto, de los Lunes y los jueves.';

CREATE TABLE servicios(
	id serial, 
	codigo text NOT NULL, 
	servicio text NOT NULL, 
	descripcion text,
	precio numeric DEFAULT 0,
	repetitivo boolean DEFAULT false,
	activo boolean DEFAULT true,
	CONSTRAINT pk_servicios PRIMARY KEY (id),
    CONSTRAINT unq_ser_cod UNIQUE (codigo)
);

CREATE TABLE insumos(
	id serial, 
	codigo text NOT NULL, 
	insumo text NOT NULL, 
	precio numeric DEFAULT 0,
	cantidad numeric DEFAULT 0,
	activo boolean DEFAULT true,
	CONSTRAINT pk_insumos PRIMARY KEY (id),
    CONSTRAINT unq_ins_cod UNIQUE (codigo)
);

CREATE TABLE ser_ins(
	id serial, 
	id_servicios int NOT NULL, 
	id_insumos int NOT NULL, 
	cantidad int DEFAULT 0,
	CONSTRAINT pk_serins PRIMARY KEY(id), 
	CONSTRAINT unq_serins_serins UNIQUE (id_servicios, id_insumos),
	CONSTRAINT fk_serins_ser FOREIGN KEY(id_servicios) REFERENCES servicios(id), 
	CONSTRAINT fk_serins_ins FOREIGN KEY(id_insumos) REFERENCES insumos(id)
);

insert into servicios(codigo, servicio, descripcion, precio, repetitivo) 
values('CA-01', 'Cambio de Aceite #1', 'Cambio de aceite y filtro(aceite)', 10000, true);

insert into servicios(codigo, servicio, descripcion, precio, repetitivo) 
values('CA-02', 'Cambio de Aceite #2', 'Cambio de aceite, filtro(aceite) y filtro de aire', 12000, true);

insert into servicios(codigo, servicio, descripcion, precio, repetitivo) 
values('AC-01', 'Revisión de A/C', 'Revisión de aire acondicionado', 10000, false);

insert into insumos(codigo, insumo, precio, cantidad) values('FC-01','Filtro Aceite', 2500, 100);
insert into insumos(codigo, insumo, precio, cantidad) values('FA-01','Filtro Aire', 5000, 10);
insert into insumos(codigo, insumo, precio, cantidad) values('AC-01','Aceite 20w50', 5500, 2000);

insert into ser_ins(id_servicios, id_insumos, cantidad) values(1, 1, 1);
insert into ser_ins(id_servicios, id_insumos, cantidad) values(1, 3, 8);

insert into ser_ins(id_servicios, id_insumos, cantidad) values(2, 1, 1);
insert into ser_ins(id_servicios, id_insumos, cantidad) values(2, 2, 1);
insert into ser_ins(id_servicios, id_insumos, cantidad) values(2, 3, 8);

--id,id_insumo, id_MARCA, ID_MODELO, ANNO_I, ANMO_F
-- 1, 1, 1, NULL, NULL, NULL;
-- 2, 2, 1, 1, NULL, NULL;
-- 3, 3, 1, 1, 2010, NULL;
-- 4, 4, 1, 1, 2010, 2019;

-- id, marca
-- id,id_marca, modelo

select s.id id_ser, 
	  s.codigo cod_ser, 
	  s.servicio, 
	  s.precio pre_ser,
	  i.id id_ins, 
	  i.codigo cod_ins, 
	  i.insumo,
	  i.precio pre_ins,  
	  si.cantidad
from servicios s  join ser_ins si on(s.id = si.id_servicios) 
join insumos i on (si.id_insumos = i.id)
where s.codigo = 'CA-01';

select 
	  s.id id_ser, 
	  s.codigo cod_ser, 
	  s.precio + sum(i.precio * si.cantidad) total_ins
from servicios s  join ser_ins si on(s.id = si.id_servicios) 
join insumos i on (si.id_insumos = i.id)
where s.codigo = 'CA-01'
group by s.id, s.codigo, s.precio;


select * from servicios
-- 1	"CA-01"	"Cambio de Aceite #1"	"Cambio de aceite y filtro(aceite)"	"10000"	true	true
select * from ser_ins
-- 1	1	1	1
-- 2	1	3	8
select * from insumos 
-- 1	"FC-01"	"Filtro Aceite"	"2500"	"100"	true
-- 3	"AC-01"	"Aceite 20w50"	"5500"	"2000"	true

select
	  s.codigo,
	  s.precio, 
	  sum(si.cantidad * i.precio) total
from servicios s  join ser_ins si on(s.id = si.id_servicios) 
join insumos i on (si.id_insumos = i.id)
--where si.id_servicios is null
group by s.precio, s.codigo



--TAREA: Interfaz de Servicios, Insumos y Relacion Servicios e Insumos. 

select * from servicios join ser_ins on(servicios.id = ser_ins.id_servicios)
select * from insumos where activo = true;
-- update from insumos set codigo = ?, insumo = ?,  precio = ?, cantidad = ? where id = ? 
-- delete
-- update from insumos set activo = false where id = ? 


select sum(precio) precios from servicios



