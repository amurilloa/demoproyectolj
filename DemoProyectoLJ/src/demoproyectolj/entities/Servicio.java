/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyectolj.entities;

/**
 *
 * @author allanmual
 */
public class Servicio {

    private int id;
    private String servicio;
    private double precio;
    private boolean activo;

    public Servicio() {
        
    }

    public Servicio(int id, String servicio, double precio, boolean activo) {
        this.id = id;
        this.servicio = servicio;
        this.precio = precio;
        this.activo = activo;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Servicio{" + "id=" + id + ", servicio=" + servicio + ", precio=" + precio + ", activo=" + activo + '}';
    }
    
    
}
